import React, { useState, useEffect } from 'react';
import C3Chart from 'react-c3js';
import 'c3/c3.css';
import { compare, average } from '../utils/helper';

let count = 1; // to force remount when axis changed.

const Chart = ({samples}) => {


    const [data, setData] = useState({
        columns: [
            ['PM2.5', 1]
        ]
    });

    const [axis, setAxis] = useState({
        x: {
            type: 'category',
            categories: ['cat1']
        }
    });

    const [avg, setAvg] = useState(0); // avg. pm2.5 of Tapiei

    useEffect(() => {
        // console.log('Samples: ', samples)
        if(typeof samples === "object") {

            samples.sort(compare);
             
            let myData = [];
            let myCate = [];

            for(let i = 0; i < samples.length; i++) {
                myData.push(samples[i]['PM2.5']);
                myCate.push(samples[i]['County'] + " " + samples[i]['SiteName'])
            } 

            setData({
                columns: [
                    ['PM2.5'].concat(myData)
                ],
                labels: true
            });

            setAxis({
                x: {
                    type: 'category',
                    tick: {
                        rotate: 75,
                        multiline: false
                    },
                    categories: myCate
                }
            });


            let pm25_taipei = [];
            samples.forEach(e => {
                if(e['County'] === "臺北市") {
                    if(e['PM2.5'] === "ND")//回傳ND時 視為回傳0
                        e['PM2.5'] = 0;
                    pm25_taipei.push(parseInt(e['PM2.5']));
                }
            });

            setAvg(average(pm25_taipei));

            count++; // trigger remount
        }        
    }, [samples]);


    return (
        <div> 
                 
            <p>臺北市平均 PM2.5: {avg}</p>
            <C3Chart key={count} data={data} axis={axis} legend={{position: 'right'}} grid={{ x: { show: true }, y: { show: true } }} />
            <p>Number of sensors: {axis.x.categories.length}</p>
        </div>
    )
}
 
export default Chart; 